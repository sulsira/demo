<?php #page specific processing
    $image = array();
    $person =  array();
    $documents =  array();
    $compounds = array();
    $house = array();
    $tenants = array();
    $rents =  array();
    $payments = array();
    $kin = array();
    if (!empty($landlord)) {
        foreach ($landlord as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
           if ($key == 'documents') {
               foreach ($value as $d => $doc) {
                    if ($doc['type'] == 'Photo') {
                       $image = $doc;
                    }else{
                        $documents[] = $doc;
                    }
               }
           }
           if ($key == 'compounds') {

                foreach ($value as $c => $compound) {
                   $compounds[] = $compound;

                   if (isset($compound['houses'])) {
                       foreach ($compound['houses'] as $h => $house) {
                           $houses[] = $house;
                           if (isset($house['tenants'])) {
                               foreach ($house['tenants'] as $t => $tenant) {
                                   if ( $tenant['tent_status'] == 1 || $tenant['tent_status'] == 2  ) {
                                      $tenants[] = $tenant;
                                      if (isset($tenant['rents'])) {
                                         foreach ($tenant['rents'] as $r => $rent) {
                                            $rents[] = $rent;
                                            if (isset($rent['payments'])) {
                                                foreach ($rent['payments'] as $p => $payment) {
                                                $payments[] = $payment;
                                                }
                                            }

                                         }
                                      }
                                   }
                               }                               
                           }

                       }
                   }
                }

           }
           if ($key == 'kin') {
               $kin = $value;
           }
        }
    }
    $contacts = (!empty($person['contacts']))? $person['contacts'] : [];
    $addresses = (!empty($person['addresses']))? $person['addresses'] : [];

    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }


    // echo '<pre style="margin:200px 80px">';
    // var_dump($tenants);
    // print_r("<hr>");
    // echo '</pre>';
    // var_dump($landlord);
    // die();
 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="{{route('land-lords.show',$landlord['id'])}}">Land lord Name : {{ucwords( $landlord['ll_fullname'] )}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#basic">General</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#compounds">Properties</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#houses">Houses</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#tenants">Tenants</a> </li>
                            <!-- <li><a href="#transactions">Transactions</a></li> -->
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#payments">Payments</a></li>
                            <li><a href="{{route('land-lords.edit',$landlord['id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul>                      
                <?php endif ?>

            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($landlord['ll_fullname'] )}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>National ID's:</td>
                                            <td> {{ucwords($person['pers_NIN'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Next of kin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($kin)): ?>

                                                    <tr>
                                                        <td>Fullname:</td>
                                                        <td><?php echo ucwords($kin['fname'].' '.$kin['mname'].' '.$kin['lname'] ) ?></td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contacts: </td>
                                                        <td>{{$kin['contacts']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                      
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div> <!-- a .cc -->
  <div class="cc clearfix" id="compounds">
    <h3>Properties</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Property</th>
            <th>Number of houses</th>
            <th>Location</th>
            <!-- <th>Available Houses</th> -->
            <th>created</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($compounds)): ?>
          <?php foreach ($compounds as $comp => $compound): ?>
            <tr>
                <td>{{ucwords($compound['name'])}}</td> 
                <td><?php echo count($compound['houses']) ?></td> 
                <td>{{ucwords($compound['location'])}}</td> 
                <td>{{$compound['created_at']}}</td> 
                <td>
                    <a href="{{route('compounds.show',$compound['comp_id'])}}">view</a> |
                    <a href="#">options</a>
                </td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>No properties yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
  <div class="cc clearfix" id="houses">
    <h3>Houses</h3>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <!-- <th>Property</th> -->
                <th>House #</th>
                <th>Tenant (current)</th>
                <th>Status</th>
                <th>price(D)</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>    
            @if(!empty($houses))   
                <?php foreach ($houses as $h => $ahouse): ?>
                     @include('__partials/modal-edit-ahouse')
                     @include('__partials/modal-add-atenant')
                    <tr>
                        <td>
                            <a href="{{route('houses.show',$ahouse['hous_id'])}}">{{$ahouse['hous_number']}}</a>
                        </td>
                        <td>
                            @if( !empty($ahouse['tenants']) )
                                <?php foreach ($ahouse['tenants'] as $tt => $tenant): ?>
                                     <a href="{{route('tenants.show', $tenant['tent_id'])}}">
                                        <?php
                                         echo ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname']);
                                        ?>
                                    </a>      
                                <?php endforeach ?>
                            @else
                                No-tenant
                            @endif
                        </td> 
                        <td>

                            @if($ahouse['hous_status'] == 1)
                                Occupied
                            @elseif($ahouse['hous_status'] == 2)
                                To vacate

                            @elseif($ahouse['hous_status'] == 0)

                                 Empty

                            @else

                                <a href="{{route('houses.show',$ahouse['hous_id'])}}">check status</a>

                            @endif

                        </td> 
                        <td>
                            {{$ahouse['hous_price']}}
                        </td> 
                        <td>
                                    <a href="#" class="dropdown-toggle"  data-toggle="dropdown">
                                        Options
                                        <!-- <span class="caret"></span> -->
                                    </a>
                                      <ul class="dropdown-menu">
                                        <!-- dropdown menu links -->
                                        <li>
                                            <a href="{{route('houses.show',$ahouse['hous_id'])}}">view</a>
                                        </li>
                                        <li>

                                            <a href="#edit_{{$ahouse['hous_id']}}" role="button" data-toggle="modal">Edit</a>
                                            
                                        </li> 
                                        @if($ahouse['hous_status'] == 0)
                                        <li>
                                            <a href="#add_{{$ahouse['hous_id']}}" role="button" data-toggle="modal">Add tenant</a>
                                        </li>
                                        @endif
       
                                        <li>
                                            {{Form::delete('houses/'. $ahouse['hous_id'], 'Delete')}}
                                        </li>
                                      </ul>
                        </td>                         
                    </tr>                   
                <?php endforeach ?>
            @else
            <tr colspan="5">
                <td>
                    <h4>No houses available</h4>
                </td>
            </tr>
            @endif
        </tbody>
    </table>       
  </div>
    <div class="cc clearfix" id="tenants">
    <h3>Tenants</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>fullname</th>
            <th>House #</th>
            <th>Deposit</th>
            <th>updated</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($compound['houses'])): ?>
          <?php foreach ($compound['houses']as $h => $house): ?>
              <?php if (!empty($house)): ?>
                  <?php foreach ($house['tenants'] as $t => $tenant): ?>

                    <tr>
                        <td>
                             <a href="{{route('tenants.show', $tenant['tent_id'])}}">
                                <?php
                                 echo ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname']);
                                ?>
                            </a> 
                        </td> 
                        <td><a href="{{route('houses.show',$house['hous_id'])}}">{{$house['hous_number']}}</a></td> 
                        <td>{{$tenant['tent_advance']}}</td> 
                        <td>{{$tenant['updated_at']}}</td> 
                        <td>{{$tenant['created_at']}}</td> 
                    </tr>                        
                  <?php endforeach ?>
              <?php endif ?>
                                            
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>No house yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
  <div class="cc clearfix" id="transactions">
    <h3>Payment Transactions</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Payer</th>
            <th>Amount</th>
            <th># Months</th>
            <th>Description</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($payments)): ?>
          <?php foreach ($tenants as $tt => $tenan): ?>
            <?php foreach ($tenan['rents'] as $ren => $rent): ?>
                <?php foreach ($rent['payments'] as $pp => $pay): ?>
                    <tr>
                        <td>
                            <a href="{{route('tenants.show', $tenan['tent_id'])}}">
                                <?php
                                 echo ucwords($tenan['person']['pers_fname'].' '.$tenan['person']['pers_mname'].' '.$tenan['person']['pers_lname']);
                                ?>
                            </a>                       
                        </td>
                        <td>
                            {{$pay['paym_paidAmount']}}
                        </td>
                        <td>
                            {{$pay['paym_forMonths']}}
                        </td>
                        <td>
                            @if($pay['paym_remarks'])
                                {{$pay['paym_remarks']}}
                            @else
                             N/A
                            @endif
                        </td>
                        <td>
                            {{$pay['created_at']}}
                        </td>
                    </tr>                    
                <?php endforeach ?>
            <?php endforeach ?>                        
            <?php endforeach ?>  
        <?php else: ?>
         <tr><td colspan="8"><h4>No Payment yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>



  <div class="cc clearfix" id="documents">
    <h3>Documents</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Title</th>
            <th>Folder</th>
            <th>Type</th>
            <th>created</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($documents)): ?>
          <?php foreach ($documents as $key => $doc): ?>
            <tr>
                <td>{{$doc['title']}}</td> 
                <td>{{$doc['foldername']}}</td> 
                <td>{{$doc['extension']}}</td> 
                <td>{{$doc['created_at']}}</td> 
                <td>
                <?php 
                    // $keywords = preg_split('/^([\\\\]+)$/', $doc['fullpath']);
                    $keywords = preg_split('/[\\/]+/', $doc['fullpath']);
                    // $keywords = explode('/', $doc['fullpath']);
                    $file = end($keywords);
                    // hack constructing url
                    $url = '';

                    if (starts_with($doc['foldername'], '/')) {

                        $folda = $keywords[count($keywords) - 2];
                        $ftype = $keywords[count($keywords) - 3];
                        $url  .= 'media'.DS.$ftype.DS.$folda.DS.'document'.DS.$file;

                    }else{
                        $url  .= $doc['foldername'];
                        if (!ends_with($doc['foldername'], '/')) {
                            $url  .= DS;
                        }
                        $url .= 'document'.DS.$file;

                    }
                 ?>
                    <?php if (strtolower($doc['extension']) == 'pdf'): ?>
                           {{HTML::link('/view?file='.urlencode($url),'view',['target'=>'_blank','class'=>'btn'])}}
                           {{HTML::link('/download?file='.urlencode($url),'Download',['target'=>'_blank','class'=>'btn'])}}
                        <?php else: ?>
                           {{HTML::link('/download?file='.urlencode($url),'Download',['target'=>'_blank','class'=>'btn'])}}
                    <?php endif ?>
                </td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
         <tr><td colspan="8"><h4>No Documents yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>



</div>
@stop
@include('templates/bottom-admin')