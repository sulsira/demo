@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-estate')
	<div class="c-header cc">
		<h3>Estate <a href="#myModal" role="button" data-toggle="modal"><i class="fa fa-plus"></i></a></h3>
	</div>
	<div class="cc clearfix">
		<div class="messages">
			@include('flash::message')
			@include('__partials/errors')
		</div>
		<hr>
        <ul class="thumbnails">
        	<?php if (!empty($estate)): ?>
        		<?php foreach ($estate as $key => $value): ?>
		          <li class="span5">
		            <div class="thumbnail">
		              <div class="caption">
		                <h3>{{ucwords($value['name'])}}</h3>
		                <hr>
		                <p>
							{{ucwords($value['location'])}}
		                </p>
		                 <span>Number of Plots:</span> <strong>{{count($value['plots'])}}</strong>
		                <hr>

		                <a href="{{route('estates.plots.create',$value['est_id'])}}" class="btn btn-primary">Add plot</a>
						<a href="{{route('estates.plots.index',[$value['est_id']])}}" class="btn <?php echo (count($value['plots']))? '' : 'disabled'?>">view plots</a>
					  	<div class="btn-group">
		                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown">Options <span class="caret"></span></button>
		                <ul class="dropdown-menu">
		                  <li><a href="{{route('estates.edit',$value['est_id'])}}" role="button" data-toggle="modal" data-type="estates" data-id="{{$value['est_id']}}">Edit estate</a></li>
		                  <!-- <li class="divider"></li> -->
							<li class="dropdown-submenu">
								{{Form::delete('estates/'. $value['est_id'], 'Delete')}}
		                    <!-- <a tabindex="-1" href="{{route('estates.edit',$value['est_id'])}}">Delete estate</a> -->
<!-- 		                    <ul class="dropdown-menu">
		                      <li><a tabindex="-1" href="#">Delete only estate</a></li>
		                      <li><a tabindex="-1" href="#">Delete with all relationships</a></li>
		                      <li><a tabindex="-1" href="#">Delete only relationship</a></li>
		                    </ul> -->
		                  </li>
		                </ul>
					    </div>
		              </div>
		            </div>
		          </li>        			
        		<?php endforeach ?>
        		<?php else: ?>
        		<h4>there are no Estate recorded yet!</h4>
        	<?php endif ?>
        </ul>
	</div>
@stop
@include('templates/bottom-admin')