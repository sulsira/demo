<!-- Modal -->

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Add a new payment</h3>
  </div>
	  {{Form::open(array('route'=>'users.store'))}}
		  <div class="modal-body">
			<div class="form-snippet">
				{{Form::open(['route'=>'customers.store'],[],['class'=>'form-snippet'])}}

					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('person[Pers_Ethnicity]','Type')}}
								<select name="person[Pers_Ethnicity]" id="enit" class="input-xlarge">
									<option>rent</option>
									<option>expense</option>
								</select>
							</div>
							<div>
								{{Form::label('person[Pers_Ethnicity]','payment to')}}
								<select name="person[Pers_Ethnicity]" id="enit" class="input-xlarge">
									<option>mamadou jallow</option>
								</select>
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Monetary </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('person[dob]','Amount')}}
								{{Form::number('person[dob]',['class'=>'input-xlarge','placeholder'=>'Enter admission date','step'=>'any'])}}
							</div>
							<div>
								{{Form::label('person[Pers_Ethnicity]','remarks')}}
								{{Form::text('address[Addr_Town]',null,['class'=>'input-xlarge','placeholder'=>'Enter town'])}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Compound details</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('plotID','Compounds')}}
								<select name="cust[cust_plotID]">
								<?php if (!empty($plots)): ?>
									<?php foreach ($plots as $key => $plot): ?>
										<option value="{{$plot['plot_id']}}">{{$plot['plot_number']}}</option>
									<?php endforeach ?>
									<?php else: ?>
									<option value="0">Not Available</option>
								<?php endif ?>
								</select>
								
							</div>
							<span>OR</span>
							<div>
								{{Form::label('cust[cust_downpayment]','Compund name')}}
								{{Form::text('cust[cust_downpayment]',null,['class'=>'input-xlarge','placeholder'=>'Enter down payment in dalasis','required'=>1,'step'=>'any'])}} 
							</div>
						</div>
					</div>
			</div>
		  </div>
			<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button class="btn btn-primary" name="createUser">Save changes</button>
			</div><!-- end of modal footer -->		
	 {{Form::close()}}
</div>