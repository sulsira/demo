<?php

class Staff extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = ['position',
'department',	'staff_personid'];

	public function documents(){
		return $this->hasMany('Document','entity_ID','id');
	}
	public function person(){
		return $this->belongsTo('Person','staff_personid','id');
	}
}