<?php

class Estate extends \Eloquent {
	protected $primaryKey = 'est_id';
	protected $fillable = ['name','location'];

	public function plots(){
		return $this->hasMany('Plot','plot_estateID');
	}
}