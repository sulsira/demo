<?php namespace services\validators;

class Payment extends Validate{
		public static $rules = [
		'paym_paidAmount'=> 'required|numeric',
		'paym_currentBal'=> 'numeric',
		'paym_transDate'=> 'required|date'
		// 'user_password'=> 'required|max:200|exists:users,password'
		// 'payment_code'=> 'required|max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}