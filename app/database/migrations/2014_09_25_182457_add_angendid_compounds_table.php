<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAngendidCompoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('compounds', function(Blueprint $table)
		{
			$table->integer('agendid')->default(0)->after('comp_landLordID')
;
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('compounds', function(Blueprint $table)
		{
			$table->dropColumn('agendid');
		});
	}

}
